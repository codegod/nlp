﻿namespace Nlp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainTextBox = new System.Windows.Forms.RichTextBox();
            this.openFileButton = new System.Windows.Forms.Button();
            this.fileDialog = new System.Windows.Forms.OpenFileDialog();
            this.listFileBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.categorizeButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.keywordsBox = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // mainTextBox
            // 
            this.mainTextBox.Location = new System.Drawing.Point(21, 38);
            this.mainTextBox.Name = "mainTextBox";
            this.mainTextBox.Size = new System.Drawing.Size(1203, 234);
            this.mainTextBox.TabIndex = 0;
            this.mainTextBox.Text = "";
            // 
            // openFileButton
            // 
            this.openFileButton.Location = new System.Drawing.Point(1135, 577);
            this.openFileButton.Name = "openFileButton";
            this.openFileButton.Size = new System.Drawing.Size(97, 37);
            this.openFileButton.TabIndex = 1;
            this.openFileButton.Text = "Open file";
            this.openFileButton.UseVisualStyleBackColor = true;
            this.openFileButton.Click += new System.EventHandler(this.OpenFileButton_Click);
            // 
            // fileDialog
            // 
            this.fileDialog.FileName = "RichText";
            this.fileDialog.Filter = "Office Files|*.doc;*.docx | Text files (*.txt)|*.txt";
            this.fileDialog.FilterIndex = 2;
            this.fileDialog.SupportMultiDottedExtensions = true;
            // 
            // listFileBox
            // 
            this.listFileBox.FormattingEnabled = true;
            this.listFileBox.ItemHeight = 16;
            this.listFileBox.Location = new System.Drawing.Point(21, 319);
            this.listFileBox.Name = "listFileBox";
            this.listFileBox.Size = new System.Drawing.Size(454, 116);
            this.listFileBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 295);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Recent files";
            // 
            // categorizeButton
            // 
            this.categorizeButton.Location = new System.Drawing.Point(12, 577);
            this.categorizeButton.Name = "categorizeButton";
            this.categorizeButton.Size = new System.Drawing.Size(97, 37);
            this.categorizeButton.TabIndex = 4;
            this.categorizeButton.Text = "Categorize";
            this.categorizeButton.UseVisualStyleBackColor = true;
            this.categorizeButton.Click += new System.EventHandler(this.CategorizeButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(507, 295);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Keywords";
            // 
            // keywordsBox
            // 
            this.keywordsBox.FormattingEnabled = true;
            this.keywordsBox.ItemHeight = 16;
            this.keywordsBox.Location = new System.Drawing.Point(510, 319);
            this.keywordsBox.Name = "keywordsBox";
            this.keywordsBox.Size = new System.Drawing.Size(454, 116);
            this.keywordsBox.TabIndex = 6;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1246, 629);
            this.Controls.Add(this.keywordsBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.categorizeButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listFileBox);
            this.Controls.Add(this.openFileButton);
            this.Controls.Add(this.mainTextBox);
            this.Name = "MainForm";
            this.Text = "Text classifier";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox mainTextBox;
        private System.Windows.Forms.Button openFileButton;
        private System.Windows.Forms.OpenFileDialog fileDialog;
        private System.Windows.Forms.ListBox listFileBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button categorizeButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox keywordsBox;
    }
}


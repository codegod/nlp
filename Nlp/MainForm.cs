﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.OleDb;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Nlp
{
    public partial class MainForm : Form
    {
        private string _connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=nlp.mdb";
        private string _stopWordsFileName = @"StopWords.txt";
        private char[] _charSeparators = new char[] { ' ' };
        private string _punctuationMarks = @"\d*?-\d|\d(?:-)|[\.,?!:;\d+–“”]";
        private string[] _stopWords = new string[] { };

        public MainForm()
        {
            InitializeComponent();
            LoadProcessedFiles();
            LoadStopWords();
            foreach (String keyword in _stopWords)
            {
                // Console.WriteLine("|"+keyword+"|");
            }
        }

        private void LoadStopWords()
        {
            if (File.Exists(_stopWordsFileName))
            {
                _stopWords = String
                    .Join(" ", File.ReadAllLines(_stopWordsFileName))
                    .Split(_charSeparators, StringSplitOptions.RemoveEmptyEntries);
            } else
            {
                MessageBox.Show("Please select stop words file.");
            }
        }

        private void LoadProcessedFiles()
        {
            DataRowCollection files = ProcessedFiles();
            foreach (DataRow file in files)
            {
                listFileBox.Items.Add(file["name"]);
            }
        }

        private DataRowCollection ProcessedFiles()
        {
            using (OleDbConnection connection = new OleDbConnection(_connectionString))
            {
                try
                {
                    connection.Open();
                    String sql = "select * from files;";
                    OleDbDataAdapter adapter = new OleDbDataAdapter(sql, connection);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    adapter.Dispose();
                    connection.Close();
                    DataTable files = ds.Tables[0];
                    return files.Rows;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    return null;
                }
            }
        }

        private void OpenFileButton_Click(object sender, EventArgs e)
        {
            if (this.fileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    mainTextBox.Text = File.ReadAllText(fileDialog.FileName);
                } catch (ArgumentException error)
                {
                    MessageBox.Show(error.Message);
                    Console.WriteLine(error);
                }
            }
        }

        private bool IsStopWord(string keyword)
        {
            return Array.IndexOf(_stopWords, keyword) == -1;
        }

        private string[] Tokenize()
        {
            String text = mainTextBox.Text.ToLower();

            // Replace newlines with spaces 
            text = Regex.Replace(text, "(\r\n|\r|\n)", " ");

            // Strip all HTML.
            text = Regex.Replace(text, "<[^<>]+>", String.Empty);

            // Strip numbers.
            text = Regex.Replace(text, "[0-9]+", String.Empty);

            // Strip urls.
            text = Regex.Replace(text, @"(http|https)://[^\s]*", "httpaddr");

            // Strip email addresses.
            text = Regex.Replace(text, @"[^\s]+@[^\s]+", "emailaddr");

            // Strip dollar sign.
            text = Regex.Replace(text, "[$]+", "dollar");

            // Strip usernames.
            text = Regex.Replace(text, @"@[^\s]+", "username");

            // Remove all punctuations marks
            text = Regex.Replace(text, _punctuationMarks, String.Empty);
            
            return text.Split(" @$/#.-:&*+=[]?!(){},''\">_<;%\\".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        }

        private void CategorizeButton_Click(object sender, EventArgs e)
        {
            string[] keywords = Tokenize()
                .Where(keyword => IsStopWord(keyword)).ToArray();

            Dictionary<string, int> repeatedWordsCount = new Dictionary<string, int>();

            foreach (String keyword in keywords)
            {
                if (repeatedWordsCount.ContainsKey(keyword))
                {
                    int value = repeatedWordsCount[keyword];
                    repeatedWordsCount[keyword] = value + 1;
                }
                else
                {
                    repeatedWordsCount.Add(keyword, 1);
                }
            }
            int max = 0;

            // Calculate average occurences
            foreach (KeyValuePair<string, int> keyword in repeatedWordsCount)
            {
                max = keyword.Value > max ? keyword.Value : max;
            }

            foreach (KeyValuePair<string, int> keyword in repeatedWordsCount)
            {
                if (keyword.Value >= max / 2)
                {
                    keywordsBox.Items.Add(keyword.Key);
                }
            }
        }
    }
}
